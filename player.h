//player.h
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the header file for the Player class. It contains the majority of the
//preprocessor directives and global variables.

#include "list.h"

#include <vector>
#include <string>
#include <fstream>
#include <cstring>
#include <iterator>
#include <list>
#include <array>
#include <cstdlib>
#include <exception>

using std::string;
using std::vector;
static const int MAX_SIZE = 100;
static const int BOARD_SIZE = 50;
static const int BOOK_SIZE = 5;


class Player
{
    public:
        Player();
        Player(const int p_num);
        ~Player();

        void display() const;
        //set player location on board
        void set_location(const int & hops);
        //move player forward from current location
        void plus_equals(const int & hops);
        //used to change current HP
        void set_hp(int hits);
        //return current HP
        int get_hp()const;
        //return player number
        int get_player() const;
        //return player location on board
        int get_location() const;

        //overload to display Player object
        friend ostream & operator<<(ostream & out, const Player & a_player);
        //overload for plus_equals() function forwards
        friend bool operator +=(Player & a_player, const int hops);
        //overload for plus_equals() function backwards
        friend void operator -=(Player & a_player, const int & hops);
        //add HP
        friend void operator +(Player & p1, const int hits);
        //decrease HP
        friend void operator -(Player & p1, const int hits);
        //if same player
        friend bool operator ==(const Player & p1, const Player & p2);
        //if not same player
        friend bool operator !=(const Player & p1, const Player & p2);
        //if player crosses finish line
        friend bool operator >=(const Player & a_player, const int spots);
        //increment turn number
        friend void operator ++(Player & a_player);
        //decrement turn number
        friend void operator --(Player & a_player);
        
    private:
        int player_num;     //Player ID
        int location;       //which spot on the board
        int turn_num;       //turn counter
        int hp;             //Hit Points or Health Points

};

