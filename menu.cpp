//menu.cpp
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the menu implementation file. I created a Board class 
//that builds a board with cards assigned to each space. I've used the array STL
//class for a "comment book" and the list STL class as a list of players.
//The player can check their own or everybody's stats. They can also read the
//commnent book or roll the die to take their turn.
//I wasn't able to connect the Board list and the Card lists, 
//so the player auto draws a card based on their current location.

#include "menu.h"



//unused idea
/*
Board::Board() : spot(0) {}
Board::Board(unique_ptr<Obstacle> & obs_card, const int olocation) : 
                spot(olocation) {}

Board::Board(unique_ptr<Challenge> & chall_card, const int clocation) : 
                spot(clocation) {}

Board::Board(unique_ptr<Random> & rand_card, const int rlocation) : 
                spot(rlocation) {}

Board::Board(const Board & a_board)
{
}

ostream & operator<<(ostream & out, const Board & the_board)
{
    the_board.display();
    return out;
}

void Board::display() const
{
    if (board_obs)
    {
        cout << *board_obs;
    }
    if (board_chall)
    {
        cout << *board_chall;
    }
    if (board_rand)
    {
        cout << *board_rand;
    }
    return;
}

const Board & Board::operator=(const Board & source)
{
    if (this == &source)
    {
        return * this;
    }

    this->spot = source.spot;
    if (source.board_obs)
        this->board_obs = source.board_obs;
    if (source.board_chall)
        this->board_chall = source.board_chall;
    if (source.board_rand)
        this->board_rand = source.board_rand;
    return;
}
*/


int main_menu()
{
    srand((unsigned) time(NULL));

    //separate Card LLs
    LL <Obstacle> o_list;
    LL <Challenge> c_list;
    LL <Random> r_list;

    //dummy Card receptacles
    Obstacle o_dummy;
    Challenge c_dummy;
    Random r_dummy;

    //exit choice
    bool leave = false;
    bool game_over = false;
    int card_num = 0;
    int option = 0;
    int calc_card = 0;
    //for notebook array
    int index = 0;
    
    //intialize player list and comment/note book
    list<Player> player_list = {1};
    array<Random, BOOK_SIZE> note_book;

    //build player list with build_players() function 
    //and start two list iterators
    build_players(player_list);
    auto it1 = player_list.begin();
    auto it2 = player_list.begin();
    ++it2;

    //read-in file to build Card subclass lists
    string read_file = "the_cards.txt";
    r_dummy.load_file(read_file, o_list, c_list, r_list);
    
    //initialize and build board LL from template
    LL <int> board_list;
    build_board(board_list);

    //main menu while loop
    while (!leave)
    {
        cout << "\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
             << "\nPlayer " << (*it1).get_player() << ", your turn!\n";
        option = menu_select();
        switch (option)
        {
            //display current player stats
            case 1: cout << (*it1);
                    break;

            // display note cards in notebook array        
            case 2: for (int i = 0; i < index; ++i)
                    {
                        cout << "\nCARD #" << i << endl << note_book[i];
                    } 
                    break;

                    
            case 3: //check if player reaches finish
                    game_over = (*it1 += roll_die());
                    //check if player has died 
                    if ((*it1).get_hp() < 1)
                    {
                        cout << "\nPlayer " << (*it1).get_player() 
                             << " has died!!\n";
                        game_over = true;
                    }
                    //throw exception
                    if(game_over)
                        return(1);

                    //uses algorithm functions to decide which card type and 
                    //card ID to pull
                    calc_card = (*it1).get_location();
                    type_calc(calc_card, card_num); 
    
                    switch (calc_card)
                    {
                        case 1: o_dummy.draw_card(*it1, card_num, o_list);
                                break;
                            
                        case 2: c_dummy.draw_card(*it1, *it2, card_num, c_list);
                                break;
                            
                        case 3: r_dummy.draw_card(*it1, *it2, card_num, index, r_list,
                                                    note_book);
                                break;
                        
                        default: cout << "\nError\n";
                                break;
                    }

                    //increment player's turn number
                    ++(*it1);

                    //check if iterators are at end-of-list
                    if (*it1 == player_list.back())
                    {
                        it1 = player_list.begin();
                    }
                    else if (*it1 != player_list.back())
                    {
                        ++it1;
                    }
                    if (*it2 == player_list.back())
                    {
                        it2 = player_list.begin();
                    }
                    else if (*it2 != player_list.back())
                    {
                        ++it2;
                    }
                    break;
                    
            case 4: //auto display loop for list class
                    for (auto elem : player_list)
                    {
                        cout << " " << elem;
                    }
                    cout << endl;
                    break;
                    
            case 5: //exit program
                    leave = true;                              
                    break;                                            
           
                   //for debugging and construction 
            /*case 6: board_list.display();
                    o_list.display();
                    r_list.display();
                    c_list.display();
                    break;*/
            
            default: cout << "Please enter a valid choice." << endl; 
        }
    }
    return(0);
}

int get_choice()
{
    int num;
    cin >> num;
    while (cin.fail())
    {
        cin.clear();
        cin.ignore(MAX_SIZE, '\n');
        cout << "Invalid input, please enter an integer : \n";
        cin >> num;
    }
    cin.clear();
    cin.ignore(MAX_SIZE, '\n');
   
    return num;
}

//main menu display
int menu_select()
{
    int option = 0;
    cout << endl;
    cout << "MAIN MENU" << endl;
    cout << "Please enter a number" << endl;
    cout << "===============================" << endl;
    cout << "1) Display Current Player Stats" << endl;
    cout << "2) Display Message Book" << endl;
    cout << "3) Roll Die" << endl;
    cout << "4) Display Stats for All Players" << endl;
    cout << "5) Exit" << endl;
    cout << "===============================" << endl;
    option = get_choice();
    cout << endl;

    return option;
}

//build player list from user selection(2-4)
void build_players(list<Player> & a_list)
{
    bool p_choice = false;
    int num_players;
    do
    {
        cout << "How many players? (2-4) ";
        num_players = get_choice();
        p_choice = true;
        Player player2(2);
        Player player3(3);
        Player player4(4);
        switch (num_players)
        {
            case 2:
                    a_list.push_back(player2);
                    break;

            case 3: 
                    a_list.push_back(player2);
                    a_list.push_back(player3);
                    break;

            case 4: 
                    a_list.push_back(player2);
                    a_list.push_back(player3);
                    a_list.push_back(player4);
                    break;

            default: cout << "\nWrong Input!\n";
                     p_choice = false;
                     break; 
        }
    }while (!p_choice);
    return;
}

int roll_die()
{
    int roll = 1 + (rand() % 6);
    cout << "You rolled " << roll << "!\n\n";
    return roll;
}

void build_board(LL<int> & the_list)
{
    for (int i = 1; i <= BOARD_SIZE; ++i)
    {
       the_list.add(i);
    }
    return;
}

//uses % to decide which card type should be assigned to which board space
void type_calc(int & calc_card, int & card_num)
{
    if (calc_card % 3 == 0)
    {
        calc_card = 2;
        card_num = 1 + (rand() % 3);
    }
    else if (calc_card % 2 == 0)
    {
        calc_card = 1;
        card_num = 1 + (rand() % 6);
    }
    else
    {
        calc_card = 3;
        card_num = 1 + (rand() % 3);
    }
    return;
}

//was trying to attach a card to each Board space...
/*
void build_board(LL<Board> & the_list, LL <Obstacle> o_list, LL <Challenge> c_list,
                    LL <Random> r_list)
{
    for (int i = 1; i <= BOARD_SIZE; ++i)
    {
        
        int card_num = 0;
        if (i % 3 == 0)
        {
            card_num = 1 + (rand() % 3);
            unique_ptr<Challenge> new_chall(new Challenge());
            new_chall->set_id(card_num);
            Board new_cspot(new_chall, i);
            the_list.add_doubly(new_cspot);
        }
        else if (i % 2 == 0)
        {
            card_num = 1 + (rand() % 5);
            //unique_ptr<Node<Obstacle>> new_obst(new Obstacle());
            //new_obst.set_id(card_num);
            //o_list.retrieve(new_obst);
            Board new_ospot(new_obst, i);
            the_list.add_doubly(new_ospot);
        }
        else 
        {
            card_num = 1 + (rand() % 3);
            unique_ptr<Random> new_rand(new Random());
            new_rand->set_id(card_num);
            Board new_rspot(new_rand, i);
            the_list.add_doubly(new_rspot);
        }

    }
    return;
}
*/

