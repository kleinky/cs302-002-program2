//list.tpp
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the template file for data storage. It contains instructions for  
//creating data structure templates to store the Card class objects or preferred 
//data type.

#pragma once
#include <iostream>
using std::cout;
using std::endl;

template <typename T>
Node<T>::Node() : data(nullptr), next(nullptr), previous(nullptr)
{
}

template <typename T>
Node<T>::Node(const T & source) : 
    data(source), next(nullptr), previous(nullptr) 
{
}

template <typename T>
Node<T>::Node(const Node<T> & source) 
{
}

//needed??
template <typename T>
Node<T>::~Node()
{
}


template <typename T>
typename Node<T>::node_ptr_type & Node<T>::get_next()
{
    return next;
}

template <typename T>
typename Node<T>::node_ptr_type & Node<T>::get_previous()
{
    return previous;
}

template <typename T>
void Node<T>::display() const
{
    cout << get_data();
    return;
}

//returns user-defined data of object
template <typename T>
T Node<T>::get_data() const
{
    return data;
}

template <typename T>
void Node<T>::set_next(node_ptr_type & new_next)
{
    next.reset();
    next = new_next;
    return;
}

template <typename T>
void Node<T>::set_previous(node_ptr_type & new_prev)
{
    previous.reset();
    previous = new_prev;
    return;
}

template <typename T>
bool Node<T>::has_next() const
{
    return next;
}


template <typename T>
LL<T>::LL() : head(nullptr), tail(nullptr), size(0)
{
}

template <typename T>
LL<T>::LL(const LL & to_copy) : head(nullptr), tail(nullptr), size(0)
{
}

template <typename T>
LL<T>::~LL()
{
}

//retrieve node wrapper function
template <typename T>
unique_ptr<Node<T>> & LL<T>::retrieve(unique_ptr<Node<T>> & to_copy)
{
    return retrieve(head, to_copy);
}
        
//was not able to fully implement
template <typename T>
unique_ptr<Node<T>> & LL<T>::retrieve(unique_ptr<Node<T>> & current, 
                                        unique_ptr<Node<T>> & to_copy)
{
    if (!head)
        return current;
    if (current->get_next() == head)
    {
        return current;
    }
    if (current->get_data() == to_copy->get_data())
    {
        return current;
    }
    return retrieve(current->get_next(), to_copy);
}

//recursively destroys list starting with head node
template <typename T>
void LL<T>::destroy(node_ptr_type & curr_head)
{
    if(curr_head)
    {
        destroy(curr_head->get_next());
        delete curr_head;
    }
    return;
}

template <typename T>
LL<T> & LL<T>::operator=(const LL<T> & src)
{
    return * this;
}

template <typename T>
void LL<T>::display() const
{
    if (!head) return;
    display(head);    
    cout << endl;
    return;
}

template <typename T>
void LL<T>::display(const node_ptr_type & current) const
{
    if (!current)
        return;
    current->display();
    if (current->get_next() == head)
    {
        return;
    }   
    return display(current->get_next());
}

template <typename T>
unique_ptr<Node<T>> & LL<T>::get_head()
{
    return head;
}

template <typename T>
void LL<T>::add(const T & to_add)
{
    add(head, to_add);
    return;
}

template <typename T>
void LL<T>::add(node_ptr_type & current, const T & to_add)
{
    unique_ptr<Node<T>> temp(new Node<T>(to_add));
    if (current == nullptr)
    {
        current = move(temp);
    }
    else
    {  
        add(current->get_next(), to_add);
    } 
    return;
}


//was working on for use of implementing doubly linked list, throws errors

/*
template <typename T>
void LL<T>::add_doubly(const T & to_add)
{
    add_doubly(head, to_add);
    return;
}

template <typename T>
void LL<T>::add_doubly(node_ptr_type & current, const T & to_add)
{
    unique_ptr<Node<T>> temp(new Node<T>(to_add));
    if (current == nullptr)
    {
        node_ptr_type temp_current = move(current);
        current->set_previous(temp_current);
        current = move(temp);
    }
    else
    {  
        add(current->get_next(), to_add);
    } 
    return;
}
*/
