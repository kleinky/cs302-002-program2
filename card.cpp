//card.cpp
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the implementation file for the Card base class and its three
//subclasses. It contains the file read-in function. This file is a bit bloated,
//but it makes sense to include all of the Card hierarchy together.

#include "card.h"


Card::Card() : name(nullptr), card_id(0), card_val(0), 
                                            a_card_type(card_type::none)
{
}

Card::Card(const char * name, const int card_id, const int card_val, 
            card_type a_card_type)  
{
}

Card::Card(const Card & a_card) : 
            name(nullptr), card_id(a_card.card_id), card_val(a_card.card_val),
            a_card_type(a_card.a_card_type)
{
    int length = strlen(a_card.name);
    name = new char[length+1];
    strcpy(name, a_card.name);
}

Card::~Card()
{
    //not needed in this version
   /*
    if(this->name)
    {
        delete [] this->name;
    }
*/
}

const Card & Card::operator=(const Card & source)
{
    if(this == &source)
    {
        return * this;
    }

    int length = strlen(source.name);
    name = new char[length+1];
    strcpy(name, source.name);
    this->card_id = source.card_id;
    this->card_val = source.card_val;
    this->a_card_type = source.a_card_type;
    return * this;
}

//not sure if this endded up being implemented
int Card::get_type() const
{
    int ret_type = 0;

    if (a_card_type == card_type::obst)
        ret_type = 1;
    else if (a_card_type == card_type::chall)
        ret_type = 2;
    else if (a_card_type == card_type::random)
        ret_type = 3;

    return ret_type;
}

int Card::get_val() const
{
    return card_val;
}

int Card::get_id() const
{
    return card_id;
}

int Card::get_data() const
{
    return get_id();
}

void Card::set_name(char * the_name)
{
    int length = strlen(the_name);
    name = new char[length+1];
    strcpy(name, the_name);
    return;
}

void Card::set_id(const int the_id)
{
    card_id = the_id;
    return;
}

void Card::set_val(const int the_value)
{
    card_val = the_value;
    return;
}

void Card::set_type(const int type)
{
    switch (type)
    {
        case 1:
            this->a_card_type = card_type::obst;
            break;
        case 2:
            this->a_card_type = card_type::chall;
            break;
        case 3:
            this->a_card_type = card_type::random;
            break;
        default:
            cout << "invalid input!!\n";
            break;
    }
    return;
}

void Card::display() const
{
    cout << "\n************************************************************\n";
    cout << "Card Name: " << this->name << endl; 
    cout << "Value: " << card_val << endl;
    cout << "Card Type: "; 
    
    switch (a_card_type)
    {
         case card_type::obst:
             cout << "obstacle\n";
             break;
         case card_type::chall:
             cout << "challenge\n";
             break;
         case card_type::random:
             cout << "random\n";
             break;
         default:
             cout << "none\n";
             break;
         cout << endl;
    }
    cout << "************************************************************\n";
    return;
}

ostream & operator<<(ostream & out, const Card & a_card)
{
    a_card.display();
    return out;
}


Obstacle::Obstacle() : 
    Card() 
{
}

Obstacle::Obstacle(const Obstacle & a_card)
    : Card(a_card)
{
}

Obstacle::~Obstacle()
{
    if(this->name)
    {
        delete [] this->name;
    }
}

const Obstacle & Obstacle::operator=(const Obstacle & source)
{
    if(this == &source)
    {
        return * this;
    }

    int length = strlen(source.name);
    name = new char[length+1];
    strcpy(name, source.name);
    this->card_id = source.card_id;
    this->card_val = source.card_val;
    this->a_card_type = source.a_card_type;
    return * this;
}

void Obstacle::draw_card(Player & a_player, int to_find, LL<Obstacle> & o_list)
{
    draw_card(a_player, to_find, o_list.get_head()); 
    return;
}

void Obstacle::draw_card(Player & a_player, int to_find, 
                                   unique_ptr<Node<Obstacle>> & current)
{
    Obstacle temp;
    temp.set_id(to_find);
    if (temp.card_id == ((*current).get_data()).get_id())
    {
        temp = (*current).get_data();
        switch (to_find)
        {    
            case 1: cout << temp.name << " in the road!!\n"
                         << "Go back " << temp.card_val << " spaces!\n";
                    a_player -= temp.card_val;
                    break;

            case 2: cout << temp.name << " launch!!\n"
                         << "Go forward " << temp.card_val << " spaces!\n";
                    a_player += temp.card_val;
                    break; 

            case 4: cout << temp.name << "!! You're sucked in and spat out " 
                         << temp.card_val << " spaces back!\n";
                    a_player -= temp.card_val;
                    break;

            case 5: cout << "Take the " << temp.name << " forward " 
                         << temp.card_val << " spaces forward!\n";
                    a_player += temp.card_val;
                    break;

            default: cout << temp.name << " trap!! Stuck for this turn!\n";
                     break; 
        }
    }
    else draw_card(a_player, to_find, current->get_next());
    return;
}


void Challenge::draw_card(Player & p1, Player & p2, int & to_find, 
                                                    LL<Challenge> & c_list)
{
    draw_card(p1, p2, to_find, c_list.get_head()); 
    return;
}

void Challenge::draw_card(Player & p1, Player & p2, int & to_find, 
                                    unique_ptr<Node<Challenge>> & current)
{
    Challenge temp;
    temp.set_id(to_find);
    if (temp.card_id == ((*current).get_data()).get_id())
    {
        temp = (*current).get_data();
        switch (to_find)
        {     
            case 1: cout << "The " << temp.name << " card! \n"
                         << "You steal " << temp.card_val << " HP from Player " 
                         << p2.get_player() << "!\n";
                    p2 - temp.card_val;
                    p1 + temp.card_val;
                    break;

            case 2: cout << "Attack Player " << p2.get_player() << " for " 
                         << temp.card_val << "HP!\n";
                    p2 - temp.card_val;
                    break; 

            case 3: cout << temp.name 
                         << "!! You lose " 
                         << temp.card_val << " HP!\n";
                    p1 - temp.card_val;
                    break;

            default: cout << "ERROR\n";
                 break; 
        }
    }
    else draw_card(p1, p2, to_find, current->get_next());
    return;
}

void Random::draw_card(Player & p1, Player & p2, int & to_find, int & index, 
        LL<Random> & r_list, array<Random, BOOK_SIZE> & book)
{
    draw_card(p1, p2, to_find, index, r_list.get_head(), book); 
    return;
}

void Random::draw_card(Player & p1, Player & p2, int & to_find, int & index, 
                        unique_ptr<Node<Random>> & current, array<Random, 
                        BOOK_SIZE> & book)
{
    Random temp;
    temp.set_id(to_find);
    if (temp.card_id == ((*current).get_data()).get_id())
    {
        int loc1 = 0;
        int loc2 = 0;
        int new_spot = 0;
        string a_message;
        temp = (*current).get_data();
        switch (to_find)
        {   
            case 1: loc1 = p1.get_location();
                    loc2 = p2.get_location();
                    new_spot = temp.switch_places(p2, loc1, loc2);
                    p1.set_location(new_spot);
                    break;

            case 2: cout << "The " << temp.name << " card!\n" 
                         << "You've been teleported to the "; 
                    p1.set_location(teleport()); 
                    cout << p1.get_location() << " boardspace!\n";
                    break; 

            case 3: if (index < BOOK_SIZE)
                    {
                        cout << "Write a message card for the notebook: ";
                        cin >> temp;
                        book[index] = temp;
                        ++index;
                    }
                    break;

            default: cout << "ERROR\n";
                     break; 
        }
    }
    else draw_card(p1, p2, to_find, index, current->get_next(), book);
    return;
}

//I believe I made this a Random fucntion because the other subclasses had identical 
//data members to the Card class 
void Random::load_file(const string file_name, 
                  LL<Obstacle> & o_list, LL<Challenge> & c_list, LL<Random> & r_list)
{
    ifstream infile(file_name);

    if(!infile)
    {
        cerr << "Failed to open " << file_name << " to read in!" << endl;   
        return;
    }
   
    do
    {  
        char the_name[MAX_SIZE] = {0};
        char ch_id[MAX_SIZE] = {0};
        char ch_val[MAX_SIZE] = {0};
        char ch_type[MAX_SIZE] = {0};
        int the_id = 0;
        int the_value = 0;
        int int_type = 0;

        infile.getline(the_name, MAX_SIZE, ',');      
        infile.getline(ch_id, MAX_SIZE, ',');
        the_id = ((*ch_id) - '0');
        infile.getline(ch_val, MAX_SIZE, ',');
        the_value = ((*ch_val) - '0');
        infile.getline(ch_type, MAX_SIZE, '\n');
        int_type = ((*ch_type) - '0');
                
        if(!infile.eof())
        {
            if (int_type == 1)
            {
                Obstacle obst_card;
                obst_card.set_name(the_name);
                obst_card.set_id(the_id);
                obst_card.set_val(the_value);
                obst_card.set_type(int_type);
            
                Obstacle new_obst(obst_card);

                o_list.add(new_obst);  
            }
            else if (int_type == 2)
            {
                Challenge chall_card;
                chall_card.set_name(the_name);
                chall_card.set_id(the_id);
                chall_card.set_val(the_value);
                chall_card.set_type(int_type);
            
                Challenge new_chall(chall_card);
                
                c_list.add(new_chall);  
            }
            else if (int_type == 3)
            {
                Random random_card;
                random_card.set_name(the_name);
                random_card.set_id(the_id);
                random_card.set_val(the_value);
                random_card.set_type(int_type);
                
                Random new_random(random_card);

                r_list.add(new_random); 
            }
            else
            {
                cout << "no dice\n";
            }
        }
    }while(!infile.eof());
    infile.close();
    return;
}

Challenge::Challenge() : 
    Card() 
{
}

Challenge::Challenge(const Challenge & a_card) 
    : Card(a_card) 
{
    //int length = strlen(a_card->name);
    //name = new char[length+1];
    //strcpy(name, the_name);
}

Challenge::~Challenge()
{
    if(this->name)
    {
        delete [] this->name;
    }
}

const Challenge & Challenge::operator=(const Challenge & source)
{
    if(this == &source)
    {
        return * this;
    }

    int length = strlen(source.name);
    name = new char[length+1];
    strcpy(name, source.name);
    this->card_id = source.card_id;
    this->card_val = source.card_val;
    this->a_card_type = source.a_card_type;
    return * this;
}

Random::Random() : 
    Card(), message(nullptr) 
{
}

Random::Random(const Random & a_card)
    : Card(a_card), message(nullptr)
{
    if (a_card.message)
    {
        int length = strlen(a_card.message);
        message = new char[length+1];
        strcpy(message, a_card.message);
    }
    else
    {
    }
}

Random::~Random()
{
    if(this->name)
    {
        delete [] this->name;
    }
    this->name = nullptr;
    if(this->message)
    {
        delete [] this->message;
    }
    this->message = nullptr;
}

const Random & Random::operator=(const Random & source)
{
    if(this == &source)
    {
        return * this;
    }

    int length = strlen(source.name);
    name = new char[length+1];
    strcpy(name, source.name);
    this->card_id = source.card_id;
    this->card_val = source.card_val;
    this->a_card_type = source.a_card_type;
    if (source.message)
    {
        length = strlen(source.message);
        message = new char[length+1];
        strcpy(message, source.message);
    }
    return * this;
}

void Random::display() const
{
    Card::display();
    cout << "\nMessage: \n" << message << endl;
    return;
}

int Random::switch_places(Player & p2, const int loc1, const int loc2)
{
    cout << "The " << name << " card! \n"
         << "You switch places with Player " << p2.get_player() 
         << "!\n";
    int new_spot1 = loc2;
    int new_spot2 = loc1; 
    p2.set_location(new_spot2);
    return new_spot1;
}

int Random::teleport() const
{
    int roll = 1 + (rand() % BOARD_SIZE);
    return roll;
}

void Random::set_message(string a_message)
{
    int length = a_message.size();
    delete [] this->message;
    message = new char[length+1];
    strcpy(message, a_message.c_str());
    return;
}

istream & operator>>(istream & in, Random & r_card)
{
    string temp;
    in >> temp;
    r_card.set_message(temp);
    return in;
}

ostream & operator<<(ostream & out, const Random & a_card)
{
    a_card.display();
    return out;
}

