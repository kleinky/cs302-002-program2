//main.cpp
//Kyle Klein - 5/15/23 - CS302-002 - Program #1
//This is my main program implementation file. This should be a pretty simple
//file that allows the client to interact with the entire program through
//menu functions. I may be able to implement more of the menu files
//through here to help with bloat.

#include "menu.h"

//user supplied terminate
void user_terminate()
{
}

int main()
{
    //install user function
    set_terminate(user_terminate);

    int i = 0;
    
    //boot program and get exit return value
    i = main_menu();

    try
    {
        if (i == 1)
            throw i;
    }

    catch (int j)
    {
        cout << "\no!o!o!o!o!o!o!o! GAME OVER !o!o!o!o!o!o!o!o\n\n";
    }
    
    return 0;
}


