//card.h
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the header file for the Card base class and its three subclasses.
//Each subclass has similar data types to assign relative to the parent class,
//but each subclass has different functions that affect the Player.
//Dynamic char *'s are included in Card name and Random message.

#pragma once
#include "list.h"
#include "player.h"

enum class card_type {none, obst, chall, random};


class Card
{
    public:
        Card();
        Card(const char * name, int card_id, const int card_val, 
                                            card_type a_card_type);
        Card(const Card & a_card);
        ~Card();
        const Card & operator=(const Card & source);

        void assign_card(LL<Card> & a_list);
        int get_type() const;
        int get_val() const;
        int get_id() const;
        int get_data() const;
        void set_name(char * a_name);
        void set_id(const int the_id);
        void set_val(const int the_value);
        void set_type(const int type);
        void display() const;
        friend ostream& operator<<(ostream& out, const Card & a_card);
    
    protected:
        char * name;            //card name
        int card_id;            //identifies the card of a class
        int card_val;           //value of card (e.g. this many spaces)
        card_type a_card_type;  //Card subclass
};


class Obstacle: public Card
{
    public:
        Obstacle();
        Obstacle(const char * name, const int card_id, int card_val, 
                card_type a_card_type); 
        Obstacle(const Obstacle & a_card);
        ~Obstacle();
        const Obstacle & operator=(const Obstacle & source);

        //ended up implementing a switch case that does essentially the same tasks
        //int forward();
        //int backward();
        //void lose_turn();
        
        void draw_card(Player & a_player, int to_find, LL<Obstacle> & o_list);

    protected:
        void draw_card(Player & a_player, int to_find, 
                                        unique_ptr<Node<Obstacle>> & current);
};


class Challenge: public Card
{
    public:
        Challenge();
        Challenge(const char * name, const int card_id, int card_val,  
                card_type a_card_type);
        Challenge(const Challenge & a_card);
        ~Challenge();
        const Challenge & operator=(const Challenge & source);

        //ended up implementing a switch case that does essentially the same tasks
        //int steal_hp();           //steal another player's HP
        //int attack();             //decrease another player's HP
        //int ambushed();           //current player loses HP
                                  
        void draw_card(Player & p1, Player & p2, int & to_find, LL<Challenge> & c_list);

    protected:
        void draw_card(Player & p1, Player & p2, int & to_find, 
                                        unique_ptr<Node<Challenge>> & current);
};


class Random: public Card
{
    public:
        Random();
        Random(const char * name, const int card_id, int card_val, 
                                                card_type a_card_type); 
        Random(const Random & a_card);
        ~Random();
        const Random & operator=(const Random & source);

        void load_file(const string file_name, 
                LL<Obstacle> & o_list, LL<Challenge> & c_list, LL<Random> & r_list);
        void display() const;
        //change places with another player
        int switch_places(Player & p2, const int loc1, const int loc2);              
        //move to a random space on the board
        int teleport() const;             
        //write a message on the card to include in the note_book
        void set_message(string a_message); 
        friend istream& operator>>(istream& out, Random & r_card);
        friend ostream& operator<<(ostream& out, const Random & a_card);
        void draw_card(Player & p1, Player & p2, int & to_find, int & index, 
                LL<Random> & r_list, array<Random, BOOK_SIZE> & book);

    protected:
        void draw_card(Player & p1, Player & p2, int & to_find, int & index, 
                unique_ptr<Node<Random>> & current, array<Random, BOOK_SIZE> & book);

        char * message;         
};
