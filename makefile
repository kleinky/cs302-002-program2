CC = g++
CPPFLAGS = -Wall -g -std=c++17

main:           menu.o card.o main.o player.o

main.o:			menu.h

menu.o:         menu.h 

player.o:		player.h card.h

card.o:			card.h player.h


.PHONY: clean
clean:      $(info ---makefile---)
			rm -f card.o menu.o main.o player.o
			rm -f main

leak:
	valgrind ./main --tool=memcheck --leak-check=full
