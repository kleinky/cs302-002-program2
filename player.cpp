//player.cpp
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the Player class implementation file. It probably contains the most 
//getter functions in the program as well as numerous operator overloads.

#include "player.h"

static const int HEALTH = 20;

Player::Player() : player_num(1), location(0), turn_num(0), hp(HEALTH)
{
}

Player::Player(const int p_num) 
    : player_num(p_num), location(0), turn_num(0), hp(HEALTH)
{
}

Player::~Player() 
{     
}

//displays stats of calling player
void Player::display() const
{
    cout << "\nPlayer " << player_num << " has " << hp << " Hit Points.\n"
         << "They are at board space " << location << " and have taken "
         << turn_num << " turn(s).\n";
}

//pass int to set players location on the board
//used for a couple of Random card types
void Player::set_location(const int & hops)
{
    location = hops;
    return;
}

//increase or decrease the calling player's current position
void Player::plus_equals(const int & hops)
{
    location = location + hops;
    return;
}

//increase or decrease the calling player's HP by value
void Player::set_hp(int hits)
{
    hp = hp + hits;
}

//return hp of calling player
int Player::get_hp() const
{
    return hp;
}

//return calling player's number
int Player::get_player() const
{
    return player_num;
}

//return calling player's location
int Player::get_location() const
{
    return location;
}

//operator can be used within program to call display() for Player object
ostream & operator<<(ostream & out, const Player & a_player)
{
    a_player.display();
    return out;
}

//operator will change calling player's location
//I also implemented the finish line output display here and didn't have time
//to change it
bool operator+=(Player & a_player, const int hops)
{
    a_player.plus_equals(hops);
    if (a_player.get_location() >= BOARD_SIZE)
    {
        cout << "\n*~*~*~*~*~*~*~*~*~*~*";
        cout << "  Player " << a_player.get_player() << "  wins!!!  ";
        cout << "*~*~*~*~*~*~*~*~*~*~*\n\n";
        return true;
    }
    return false;
}

//uses the same idea as above overload, but implements algorithm to change
//'hops' to negative value
void operator-=(Player & a_player, const int & hops)
{
    int back_hops = hops - (hops * 2);
    a_player.plus_equals(back_hops);
    return;
}

//add HP to player
void operator+(Player & p1, const int hits)
{
    p1.set_hp(hits);
    return;
}

//uses same negative value algorithm as above to decrease player HP
void operator-(Player & p1, int hits)
{
    hits = 0 - hits;
    p1.set_hp(hits);
    return;
}

bool operator==(const Player & p1, const Player & p2)
{
    return (p1.get_player() == p2.get_player());
}

bool operator!=(const Player & p1, const Player & p2)
{
    return (p1.get_player() != p2.get_player());
}

bool operator>=(const Player & a_player, const int spots)
{
    return (a_player.get_location() >= spots);
}

void operator++(Player & a_player)
{
    ++(a_player.turn_num);
    return;
}

void operator--(Player & a_player)
{
    --(a_player.turn_num);
    return;
}


