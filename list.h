//list.h
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//List.h is my "first" header file that the other files include and draw from, 
//but list.h must include list.tpp. This file has the templates to structure a 
//linked list with the client's choice of data type.

#pragma once

#include <iostream>
#include <memory>

using namespace std;
using std::unique_ptr;

//list node template
template <typename T>
class Node
{
    public:
        typedef Node<T> * node_ptr;
        typedef unique_ptr<Node<T>> node_ptr_type; 

        Node<T>();
        Node(const T & source);
        Node(const Node<T> & source);
        ~Node();

        void display() const;
        void set_next(node_ptr_type & new_next);
        void set_previous(node_ptr_type & new_prev);
        bool has_next() const;
        
        //better to pass by ref or return?
        node_ptr_type & get_next();
        node_ptr_type & get_previous();
        
        //return object data
        T get_data() const;

    private:
        T data;
        node_ptr_type next;
        node_ptr_type previous;
};


template <typename T>
class LL
{
    public:
        typedef Node<T> node_type;
        typedef node_type * node_ptr;
        typedef unique_ptr <node_type> node_ptr_type;
        
        LL();
        LL(const LL<T> & to_copy);
        LL<T> & operator=(const LL<T> & src);
        ~LL();

        void display() const;
        //public wrapper
        void add(const T & to_add);
        //void add_doubly(const T & to_add);
        bool remove_node(const string to_remove);
        node_ptr_type & retrieve(node_ptr_type & to_copy);
        node_ptr_type & get_head();

    protected:
        node_ptr_type head;
        node_ptr_type tail;

        int size;
        const static int INIT_CAP = 4;
        
        node_ptr_type & retrieve(node_ptr_type & current, node_ptr_type & to_copy);
        //creates a unique ptr to a new T Node from the data by reference
        //and adds it to the list
        void add(node_ptr_type & root, const T & data);
        //void add_doubly(node_ptr_type & root, const T & data);
        bool remove_node(node_ptr_type & current, const string to_remove); 
        //destroy list
        void destroy(node_ptr_type & curr_head);
        void display(const node_ptr_type & current) const;
};

#include "list.tpp"
