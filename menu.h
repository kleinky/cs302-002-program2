//menu.h
//Kyle Klein - 5/15/23 - CS302-002 - Program #2
//This is the header file for the menu function declarations as well as the 
//Player class. Player will have a pretty simple job for phase 1, and I 
//wrote most of the menu work here so the main.cpp file will just call a few 
//functions.


#pragma once
#include "card.h"


//unused idea for Board class that would implement a card on each position
//of the game board
/*
class Board
{
    public:
        Board();
        Board(unique_ptr<Obstacle> & obs_card, const int olocation); 
        Board(unique_ptr<Challenge> & chall_card, const int clocation); 
        Board(unique_ptr<Random> & rand_card, const int rlocation);
        Board(const Board & a_board);
        friend ostream & operator<<(ostream & out, const Board & the_board);
        const Board & operator=(const Board & source) = delete;
        ~Board() = default;
        void display() const;

    private:
        unique_ptr<Obstacle> board_obs;
        unique_ptr<Challenge> board_chall;
        unique_ptr<Random> board_rand;
        int spot;
};*/

//menu functions
//main menu function that runs most of the program abstractly
int main_menu();
int get_choice();
int menu_select();
//build player STL list
void build_players(list<Player> & a_list);
//uses rand() 1-6 to "roll die"
int roll_die();
// currently builds int board
// there is a broken function in .cpp working to build board for above class
void build_board(LL<int> & the_list);
//calculate which card type should be used for which board space
void type_calc(int & calc_card, int & card_num);

